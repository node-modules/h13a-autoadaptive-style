var synesthesia = require('synesthesia');
function Operation (color) {
  this.colorize = color.config.colorize;
}
Operation.prototype.transform = function( decl, options ) {
    var originalCSSValueLine = decl.value;
    // cross the streams.
    var transformedCSSValueLine = originalCSSValueLine.replace(/\s+/g, ' ');
    originalCSSValueLine = originalCSSValueLine.replace(/\-/g, 'hyphenptotectforsynth');

    // CALCULATE
    transformedCSSValueLine = originalCSSValueLine.replace(synesthesia.all, function(match, capture1, offset, string) {
      if( options && options.force ) {
        return options.force;
      } else if(match.match(/^\d$/)){
        return match;
      } else {
        return this.colorize( match, decl );
      }
    }.bind(this));
    var transformedCSSValueLine = transformedCSSValueLine.replace(/hyphenptotectforsynth/g, '-');
    return transformedCSSValueLine;
};
module.exports = Operation;
