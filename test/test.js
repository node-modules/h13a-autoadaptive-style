var Transformer = require('../lib/strategy2/index.js');

// SAMPLE color.config.js
var ColorConfigJS = {
  colorize: function (originalColor) {
    var filteredColor = (originalColor);
    console.log('[%s] --> [%s]', originalColor, filteredColor);
    return filteredColor;
  }
}

var transformer = new Transformer({
  config: ColorConfigJS
});

var assert = require('assert');

var sampleColors = [
  '#fff',
  'red',
  'green',
  'blue',
  'hsl(255,10%,10%)',
  'hsla(255,10%,10%,0.1)',
  'rgb(255,255,255)',
  'rgba(255,255,255,0.1)',
  '#AAAAAA',
  '#AAAAAA',
  'red',
  'green',
  'blue',
  '#FF0000',
  '#00FF00',
  '#0000FF',
  '#F00',
  '#0F0',
  '#00F',
  'rgb(255, 0, 0)',
  'rgb(0, 255, 0)',
  'rgb(0, 0, 255)',
  'rgba(255, 0, 0, 0.5)',
  'rgba(0, 255, 0, 0.5)',
  'rgba(0, 0, 255, 0.5)',
  'hsv(0, 100%, 100%)',
  'hsv(120, 100%, 100%)',
  'hsv(240, 100%, 100%)',
  'hsva(0, 100%, 100%, 0.5)',
  'hsva(120, 100%, 100%, 0.5)',
  'hsva(240, 100%, 100%, 0.5)',
  'hsl(0, 100%, 100%)',
  'hsl(120, 100%, 100%)',
  'hsl(240, 100%, 100%)',
  'hsla(0, 100%, 100%, 0.5)',
  'hsla(120, 100%, 100%, 0.5)',
  'hsla(240, 100%, 100%, 0.5)'
]

describe('Normal Tests', function () {
  describe('general parsing', function () {
    it('should return -1 when the value is not present', function () {
      var strings = sampleColors.join(" and ");
      var expected = sampleColors.map(function(i){ return "RED"}).join(" and ");
      assert.equal(expected, transformer.transform( {value: strings}, {force: 'RED'} ));
    });
  });
});

//console.log('red -> %s', transformer.transform( "hello red green blue" ) );
//console.log('red -> %s', transformer.transform( "hello red green blue world" ) );
//console.log('red -> %s', transformer.transform( "inset 0 1px 0 green,0 1px 0 rgba(255,255,255,0.1)" ) );
//console.log('red -> %s', transformer.transform( "hello #AAAAAA #BBBBBBBB 1px solid rgba (0,0,0,0.6) rgb (0,0 ,0,) hsla(0,0,0,0.6) red hsl( 0,0,0,) world" ) );
//console.log('red -> %s', transformer.transform( "hello linear-gradient(45deg, rgba(255,255,255,0.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,0.15) 50%, rgba(255,255,255,0.15) 75%, transparent 75%, transparent) world" ) );
