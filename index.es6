var postcss = require('postcss');
var Transformer = require('./lib/strategy2/index.js');

module.exports = {

	transform: function(o) {

		var transformer = new Transformer( o ); // <-- NOTE: transform options are inserted into Transformer

		var config = o.config;
		var source = o.source;
		var done = o.done;

		var processor = postcss(function(css){
			css.walkRules(function (rule) {
				rule.walkDecls(function (decl, i) {

						//NOTE: this is a raw value line that may contain a bunch of things
						output = transformer.transform( decl );

						decl.value = output;

					}); // walk declarations
				}); // walk rules
		}); // end post css

		processor.process(o.source).then(function(result){
			done(null, result.css);
		});

 },

 }; // exports
